

var express = require("express");
var path = require("path");


var app = express();


const NODE_PORT = 8080;
const CLIENT_FOLDER = path.join(__dirname, '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');

//(1)
app.use(express.static(CLIENT_FOLDER));

//The request/response cycle is only 1 time. Therefore it starts off with the static resource (1), if it is found, it will send that.
//If it is not found, it will move onto the error handles (2)
//The server error (3) is the catch all. It will be issued if both (1) and (2) does not.


//(2)ERROR HANDLERS
app.use(function(req, res){
    res
        .status(404)
        .sendFile(path.join(MSG_FOLDER, '/404.html'));
});

//(3) Handles server error
app.use(function(err, req, res, next) {
  console.log(err);
  res
    .status(500)
    .sendFile(path.join(MSG_FOLDER, "/500.html"));
});


app.listen(NODE_PORT, function(){
    console.log("Welcome to port: " + NODE_PORT)
})